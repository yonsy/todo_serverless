const { DynamoDBClient, GetItemCommand } = require('@aws-sdk/client-dynamodb');
const { marshall, unmarshall } = require('@aws-sdk/util-dynamodb');
const sinon = require('sinon');
const { expect } = require('chai');
const getTodo = require('../handlers/getTodo');

describe('getTodo', () => {
  let dynamodbClientStub;

  beforeEach(() => {
    dynamodbClientStub = sinon.stub(DynamoDBClient.prototype, 'send');
  });

  afterEach(() => {
    dynamodbClientStub.restore();
  });

  it('should return a todo item if it exists', async () => {
    const event = {
      pathParameters: {
        id: 'todo-id',
      },
    };

    const todoItem = {
      id: 'todo-id',
      title: 'Test Todo',
      completed: false,
      metadata: { key: 'value' },
    };

    dynamodbClientStub.resolves({ Item: marshall(todoItem) });

    const result = await getTodo.handler(event);

    expect(result.statusCode).to.equal(200);
    expect(JSON.parse(result.body)).to.deep.equal(todoItem);

    sinon.assert.calledOnce(dynamodbClientStub);
    sinon.assert.calledWithExactly(dynamodbClientStub, sinon.match.instanceOf(GetItemCommand));
  });

  it('should return 404 if todo item is not found', async () => {
    const event = {
      pathParameters: {
        id: 'nonexistent-id',
      },
    };

    dynamodbClientStub.resolves({});

    const result = await getTodo.handler(event);

    expect(result.statusCode).to.equal(404);
    expect(JSON.parse(result.body)).to.deep.equal({ error: 'Todo not found' });

    sinon.assert.calledOnce(dynamodbClientStub);
    sinon.assert.calledWithExactly(dynamodbClientStub, sinon.match.instanceOf(GetItemCommand));
  });

});
