const { DynamoDBClient, ScanCommand } = require('@aws-sdk/client-dynamodb');
const { marshall, unmarshall } = require('@aws-sdk/util-dynamodb');
const sinon = require('sinon');
const { expect } = require('chai');
const listTodos = require('../handlers/listTodos');

describe('listTodos', () => {
  let dynamodbClientStub;

  beforeEach(() => {
    dynamodbClientStub = sinon.stub(DynamoDBClient.prototype, 'send');
  });

  afterEach(() => {
    dynamodbClientStub.restore();
  });

  it('should return a list of todo items', async () => {
    const todoItems = [
      {
        id: 'todo-1',
        title: 'Test Todo 1',
        completed: false,
        metadata: { key: 'value1' },
      },
      {
        id: 'todo-2',
        title: 'Test Todo 2',
        completed: true,
        metadata: { key: 'value2' },
      },
    ];

    dynamodbClientStub.resolves({ Items: todoItems.map(marshall) });

    const result = await listTodos.handler();

    expect(result.statusCode).to.equal(200);
    expect(JSON.parse(result.body)).to.deep.equal(todoItems);

    sinon.assert.calledOnce(dynamodbClientStub);
    sinon.assert.calledWithExactly(dynamodbClientStub, sinon.match.instanceOf(ScanCommand));
  });

});
