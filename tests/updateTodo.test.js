const { DynamoDBClient, UpdateItemCommand } = require('@aws-sdk/client-dynamodb');
const { marshall, unmarshall } = require('@aws-sdk/util-dynamodb');
const sinon = require('sinon');
const { expect } = require('chai');
const updateTodo = require('../handlers/updateTodo');

describe('updateTodo', () => {
  let dynamodbClientStub;

  beforeEach(() => {
    dynamodbClientStub = sinon.stub(DynamoDBClient.prototype, 'send');
  });

  afterEach(() => {
    dynamodbClientStub.restore();
  });

  it('should update a todo item', async () => {
    const event = {
      pathParameters: {
        id: 'todo-id',
      },
      body: JSON.stringify({
        title: 'Updated Todo',
        completed: true,
        metadata: { key: 'updated' },
      }),
    };

    const updatedTodo = {
      id: 'todo-id',
      title: 'Updated Todo',
      completed: true,
      metadata: { key: 'updated' },
    };

    dynamodbClientStub.resolves({ Attributes: marshall(updatedTodo) });

    const result = await updateTodo.handler(event);

    expect(result.statusCode).to.equal(200);
    expect(JSON.parse(result.body)).to.deep.equal(updatedTodo);

    sinon.assert.calledOnce(dynamodbClientStub);
    sinon.assert.calledWithExactly(dynamodbClientStub, sinon.match.instanceOf(UpdateItemCommand));
  });

});
