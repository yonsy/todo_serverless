const { DynamoDBClient, PutItemCommand } = require('@aws-sdk/client-dynamodb');
const { marshall } = require('@aws-sdk/util-dynamodb');
const sinon = require('sinon');
const { expect } = require('chai');
const createTodo = require('../handlers/createTodo');

describe('createTodo', () => {
  let dynamodbClientStub;

  beforeEach(() => {
    dynamodbClientStub = sinon.stub(DynamoDBClient.prototype, 'send');
  });

  afterEach(() => {
    dynamodbClientStub.restore();
  });

  it('should create a new todo item', async () => {
    const event = {
      body: JSON.stringify({
        title: 'Test Todo',
        metadata: { key: 'value' },
      }),
    };

    dynamodbClientStub.resolves({});

    const result = await createTodo.handler(event);

    expect(result.statusCode).to.equal(201);
    expect(JSON.parse(result.body)).to.have.property('id');
    expect(JSON.parse(result.body)).to.have.property('title', 'Test Todo');
    expect(JSON.parse(result.body)).to.have.property('completed', false);
    expect(JSON.parse(result.body)).to.have.property('metadata').that.eql({ key: 'value' });

    sinon.assert.calledOnce(dynamodbClientStub);
    sinon.assert.calledWithExactly(dynamodbClientStub, sinon.match.instanceOf(PutItemCommand));
  });

});
