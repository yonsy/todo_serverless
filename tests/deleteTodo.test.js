const { DynamoDBClient, GetItemCommand, DeleteItemCommand } = require('@aws-sdk/client-dynamodb');
const { marshall, unmarshall } = require('@aws-sdk/util-dynamodb');
const sinon = require('sinon');
const { expect } = require('chai');
const deleteTodo = require('../handlers/deleteTodo');

describe('deleteTodo', () => {
  let dynamodbClientStub;

  beforeEach(() => {
    dynamodbClientStub = sinon.stub(DynamoDBClient.prototype, 'send');
  });

  afterEach(() => {
    dynamodbClientStub.restore();
  });

  it('should delete a completed todo item', async () => {
    const event = {
      pathParameters: {
        id: 'todo-id',
      },
    };

    const todoItem = {
      id: 'todo-id',
      title: 'Test Todo',
      completed: true,
      metadata: { key: 'value' },
    };

    dynamodbClientStub.onFirstCall().resolves({ Item: marshall(todoItem) });
    dynamodbClientStub.onSecondCall().resolves({});

    const result = await deleteTodo.handler(event);

    expect(result.statusCode).to.equal(204);

    sinon.assert.calledTwice(dynamodbClientStub);
    sinon.assert.calledWithExactly(dynamodbClientStub.firstCall, sinon.match.instanceOf(GetItemCommand));
    sinon.assert.calledWithExactly(dynamodbClientStub.secondCall, sinon.match.instanceOf(DeleteItemCommand));
  });

  it('should return 400 if todo item is not completed', async () => {
    const event = {
      pathParameters: {
        id: 'todo-id',
      },
    };

    const todoItem = {
      id: 'todo-id',
      title: 'Test Todo',
      completed: false,
      metadata: { key: 'value' },
    };

    dynamodbClientStub.resolves({ Item: marshall(todoItem) });

    const result = await deleteTodo.handler(event);

    expect(result.statusCode).to.equal(400);
    expect(JSON.parse(result.body)).to.deep.equal({ error: 'Todo must be completed before deletion' });

    sinon.assert.calledOnce(dynamodbClientStub);
    sinon.assert.calledWithExactly(dynamodbClientStub, sinon.match.instanceOf(GetItemCommand));
  });

});
