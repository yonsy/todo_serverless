const { DynamoDBClient } = require('@aws-sdk/client-dynamodb');

const dynamodbClient = new DynamoDBClient();

module.exports = dynamodbClient;
