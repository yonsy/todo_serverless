class Todo {
  constructor(id, title, completed, metadata) {
    this.id = id;
    this.title = title;
    this.completed = completed;
    this.metadata = metadata;
  }
}

module.exports = Todo;
