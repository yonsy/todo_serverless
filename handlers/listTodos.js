const { DynamoDBClient, ScanCommand } = require('@aws-sdk/client-dynamodb');
const { unmarshall } = require('@aws-sdk/util-dynamodb');

const dynamodbClient = new DynamoDBClient();

module.exports.handler = async () => {
  try {
    const result = await dynamodbClient.send(new ScanCommand({
      TableName: process.env.DYNAMODB_TABLE,
    }));

    return {
      statusCode: 200,
      body: JSON.stringify(result.Items.map(unmarshall)),
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      body: JSON.stringify({ error: 'Internal Server Error' }),
    };
  }
};
