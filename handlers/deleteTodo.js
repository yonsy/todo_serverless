const { DynamoDBClient, GetItemCommand, DeleteItemCommand } = require('@aws-sdk/client-dynamodb');
const { marshall, unmarshall } = require('@aws-sdk/util-dynamodb');

const dynamodbClient = new DynamoDBClient();

module.exports.handler = async (event) => {
  try {
    const { id } = event.pathParameters;

    const result = await dynamodbClient.send(new GetItemCommand({
      TableName: process.env.DYNAMODB_TABLE,
      Key: marshall({ id }),
    }));

    if (!result.Item) {
      return {
        statusCode: 404,
        body: JSON.stringify({ error: 'Todo not found' }),
      };
    }

    const todo = unmarshall(result.Item);

    if (!todo.completed) {
      return {
        statusCode: 400,
        body: JSON.stringify({ error: 'Todo must be completed before deletion' }),
      };
    }

    await dynamodbClient.send(new DeleteItemCommand({
      TableName: process.env.DYNAMODB_TABLE,
      Key: marshall({ id }),
    }));

    return {
      statusCode: 204,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      body: JSON.stringify({ error: 'Internal Server Error' }),
    };
  }
};
