const { DynamoDBClient, PutItemCommand } = require('@aws-sdk/client-dynamodb');
const { marshall } = require('@aws-sdk/util-dynamodb');
const { v4: uuidv4 } = require('uuid');

const dynamodbClient = new DynamoDBClient();

module.exports.handler = async (event) => {
  try {
    const { title, metadata } = JSON.parse(event.body);

    if (!title) {
      return {
        statusCode: 400,
        body: JSON.stringify({ error: 'Title is required' }),
      };
    }

    const todo = {
      id: uuidv4(),
      title,
      completed: false,
      metadata,
    };

    await dynamodbClient.send(new PutItemCommand({
      TableName: process.env.DYNAMODB_TABLE,
      Item: marshall(todo),
    }));

    return {
      statusCode: 201,
      body: JSON.stringify(todo),
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      body: JSON.stringify({ error: 'Internal Server Error' }),
    };
  }
};
