const { DynamoDBClient, UpdateItemCommand } = require('@aws-sdk/client-dynamodb');
const { marshall, unmarshall } = require('@aws-sdk/util-dynamodb');

const dynamodbClient = new DynamoDBClient();

module.exports.handler = async (event) => {
  try {
    const { id } = event.pathParameters;
    const { title, completed, metadata } = JSON.parse(event.body);

    const updateExpression = [];
    const expressionAttributeValues = {};

    if (title) {
      updateExpression.push('title = :title');
      expressionAttributeValues[':title'] = title;
    }
    if (completed !== undefined) {
      updateExpression.push('completed = :completed');
      expressionAttributeValues[':completed'] = completed;
    }
    if (metadata) {
      updateExpression.push('metadata = :metadata');
      expressionAttributeValues[':metadata'] = metadata;
    }

    if (updateExpression.length === 0) {
      return {
        statusCode: 400,
        body: JSON.stringify({ error: 'No fields to update' }),
      };
    }

    const result = await dynamodbClient.send(new UpdateItemCommand({
      TableName: process.env.DYNAMODB_TABLE,
      Key: marshall({ id }),
      UpdateExpression: `SET ${updateExpression.join(', ')}`,
      ExpressionAttributeValues: marshall(expressionAttributeValues),
      ReturnValues: 'ALL_NEW',
    }));

    return {
      statusCode: 200,
      body: JSON.stringify(unmarshall(result.Attributes)),
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      body: JSON.stringify({ error: 'Internal Server Error' }),
    };
  }
};
