# Serverless Todo API

This project is a RESTful API for managing a todo list using the Serverless Framework with AWS Lambda, API Gateway, and DynamoDB. It provides endpoints for creating, retrieving, updating, and deleting todo items.

## Project Structure

- `serverless.yml`: Serverless Framework configuration file
- `package.json`: node.js dependencies/scripts
- `handlers`: directory with the basic handlers API definition
- `tests`: directory with the unit tests
- `utils`: directory for util js files used
- `models`: directory with the Todo model definion (not used actually)

## Prerequisites

Before getting started, make sure you have the following installed:

- Node.js (version 18.x)
- npm (version 8.x)
- AWS CLI (configured with your AWS credentials)
- Serverless Framework (version 3.x)

## Setup

Follow these steps to set up the project:

1. Clone the repository:

```
https://gitlab.com/yonsy/todo_serverless
```

2. Navigate to the project directory:

```
cd todo_serverless
```

3. Install the project dependencies:

```
npm install
```

4. Configure your AWS credentials:
- Make sure you have the AWS CLI installed and configured with your AWS access key and secret access key.
- Run `aws configure` and follow the prompts to enter your AWS credentials and default region.

5. Update the `serverless.yml` file:
- Open the `serverless.yml` file and review the configuration.
- Modify any necessary settings, such as the AWS region, stage, or function names.

6. Deploy the API to AWS:

```
serverless deploy
```

his command will deploy your Serverless Todo API to AWS using the configured settings.

7. Once the deployment is complete, you will see the API endpoints and other relevant information in the terminal output.

## Running Tests

The project includes a set of unit tests to ensure the functionality of the API endpoints and handlers. To run the tests, follow these steps:

1. Make sure you have completed the setup steps mentioned above.

2. Run the tests using the following command:

```
npm test
```

This command will execute the test files located in the `tests` directory using the Mocha test framework.

3. The test results will be displayed in the terminal, indicating the number of passing and failing tests.

4. If any tests fail, the terminal output will provide details about the specific test cases that failed and the reasons for the failure.

5. Review the test files in the `tests` directory to understand the test cases and make any necessary modifications.

6. If you make changes to the code, re-run the tests to ensure that everything is still functioning as expected.

## API Endpoints

After deploying the API, you can use the following endpoints to interact with the Todo API:

- `POST /todos`: Create a new todo item.
- `GET /todos/{id}`: Retrieve a specific todo item by ID.
- `GET /todos`: Retrieve a list of all todo items.
- `PUT /todos/{id}`: Update a todo item by ID.
- `DELETE /todos/{id}`: Delete a todo item by ID.

Refer to the `serverless.yml` file for more details on the request and response formats for each endpoint.

You can launch locally the endpoints, to do it you need to run the following command:

```
serverless offline
```

## Cleanup

To remove the deployed API and its associated resources from AWS, run the following command:

```
serverless remove
```
